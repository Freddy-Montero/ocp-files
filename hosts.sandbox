[OSEv3:vars]
# This variable configures the subnet in which services will be created within
# the OpenShift Origin SDN. This network block should be private and must
# not conflict with any existing network blocks in your infrastructure to
# which pods, nodes, or the master may require access to, or the installation
# will fail. Defaults to 172.30.0.0/16, and cannot be re-configured after
# deployment.
openshift_portal_net=10.65.0.0/16

# This variable overrides the SDN cluster network CIDR block. This is
# the network from which pod IPs are assigned. This network block should
# be a private block and must not conflict with existing network blocks
# in your infrastructure to which pods, nodes, or the master may require
# access.
osm_cluster_network_cidr=100.64.0.0/16

# DNS related entries that differ for each cluster
#DNS for Master & WebUI
openshift_master_cluster_hostname=master-console-sbx.tld.xxx
openshift_master_cluster_public_hostname=master-console-sbx.tld.xxx
# Wildcard DNS
openshift_master_default_subdomain=apps-sbx.tld.xxx
openshift_console_hostname=console.apps-sbx.tld.xxx
openshift_logging_kibana_hostname=kibana.apps-sbx.tld.xxx
openshift_hosted_registry_hostname=registry.apps-sbx.tld.xxx
openshift_hosted_registry_console_hostname=registry-console.apps-sbx.tld.xxx

### Environment Specific ###
# How many Routers per Environment given # of Infra Nodes
num_routers=1

# Is Proxy needed ?
openshift_proxy_auth=
openshift_proxy_server=

# Master HaProxy & KeepAlive VIP
haproxy_masters_vip=10.20.1.30

# Application HaProxy & KeepAlive VIP
haproxy_apps_vip=10.20.1.31

#############################

# Create an OSEv3 group that contains all the main groups
[OSEv3:children]
etcd
masters
infras
appnodes
glusterfs

# Each master node should also be configured for etcd
[etcd:children]
masters

[nodes:children]
masters
infras
appnodes
glusterfs

[masters]
nodem301.tld.xxx openshift_node_group_name="node-config-master"
nodem302.tld.xxx openshift_node_group_name="node-config-master"
nodem303.tld.xxx openshift_node_group_name="node-config-master"

[infras]
nodei304.tld.xxx openshift_node_group_name="node-config-infra"
nodei305.tld.xxx openshift_node_group_name="node-config-infra"
nodei306.tld.xxx openshift_node_group_name="node-config-infra"

[appnodes]
nodea307.tld.xxx openshift_node_group_name="node-config-compute"
nodea308.tld.xxx openshift_node_group_name="node-config-compute"
nodea309.tld.xxx openshift_node_group_name="node-config-compute"

[glusterfs]
nodea310.tld.xxx openshift_node_group_name="node-config-infra" glusterfs_ip=xx.xx.xx.15 glusterfs_devices='[ "/dev/sdc" ]'
nodea311.tld.xxx openshift_node_group_name="node-config-infra" glusterfs_ip=xx.xx.xx.16 glusterfs_devices='[ "/dev/sdc" ]'
nodea312.tld.xxx openshift_node_group_name="node-config-infra" glusterfs_ip=xx.xx.xx.17 glusterfs_devices='[ "/dev/sdc" ]'

# These groups are needed for HaProxy & KeepAlive (Master & Application)
[master_lb]
haproxy1.tld.xxx
haproxy2.tld.xxx

[app_lb]
haproxy3.tld.xxx
haproxy4.tld.xxx

# if all in one
[all_lb]
haproxy1.tld.xxx
haproxy2.tld.xxx
