#!/bin/bash
# Script to be called by keepalived when notify events happen

if [ -z "$1" ]; then
    echo "Error: Please provide an action"
    exit 1
fi

case $1 in
    master)
        usr/bin/systemctl start haproxy.service 
        ;;
    backup)
        usr/bin/systemctl stop haproxy.service 
        ;;
    fault)
        usr/bin/systemctl stop haproxy.service 
        ;;
    stop)
        usr/bin/systemctl stop haproxy.service 
        ;;
    *)
        echo "UKNOWN STATUS: $1"
        exit 1
        ;;
esac

exit 0
