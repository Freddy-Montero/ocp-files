# OpenShift Container Platform Inventory & Files

### Maintained by:
```
Created: 2019-05-01
Freddy Montero
```

### Templates and files for OCP >= 3.10
1. Read and go-through the PreReqs (either OCP3x-Prereqs.pdf or OCP3x-Prereqs.xlsx) document to have all the require components ready
2. Clone this repository into your ansible utility host as
```
git clone http://git_url/user/repository.git /etc/ansible/ocp
```

#### Tree Structure
```
├── LICENSE
├── OCP3x-Prereqs.pdf
├── OCP3x-Prereqs.xlsx
├── README.md
├── ansible.cfg
├── config.sh
├── config.yml
├── group_vars
│   ├── OSEv3
│   │   ├── audit.yml
│   │   ├── auth.yml
│   │   ├── cluster-monitoring.yml
│   │   ├── docker.yml
│   │   ├── logging.yml
│   │   ├── main.yml
│   │   ├── masters.yml
│   │   ├── metrics.yml
│   │   ├── network.yml
│   │   ├── project-limits.yml
│   │   ├── proxy.yml
│   │   ├── redhat-registry.yml
│   │   ├── registry.yml
│   │   ├── router.yml
│   │   └── service-catalog.yml
│   ├── appnodes
│   │   └── main.yml
│   ├── infras
│   │   └── main.yml
│   └── masters
│       └── main.yml
├── hosts.sandbox
├── pre-requisites.yml
├── provision
│   ├── apps
│   │   ├── app1.yml
│   │   └── template.yml
│   └── base
│       ├── ocp.yml
│       └── sandbox.yml
├── provision-ocp-application-projects.sh
├── provision-ocp-base-projects.sh
├── provision.yml
├── resources
│   ├── clusterroles
│   │   └── imagestream-manager.yml
│   ├── imagestreams
│   │   └── rhel7.yml.j2
│   ├── secrets
│   └── templates
│       └── project-request.template.yml.j2
├── roles
│   ├── haproxy-keepalived
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── files
│   │   ├── handlers
│   │   │   └── main.yml
│   │   ├── tasks
│   │   │   ├── configure_keepalived.yml
│   │   │   ├── firewalld.yml
│   │   │   ├── install.yml
│   │   │   └── main.yml
│   │   ├── templates
│   │   │   ├── keepalived.conf.j2
│   │   │   └── notify.sh
│   │   └── vars
│   ├── node-prep
│   │   ├── defaults
│   │   ├── files
│   │   ├── handlers
│   │   ├── meta
│   │   ├── tasks
│   │   │   ├── appnode-fs.yml
│   │   │   ├── create-partition.yml
│   │   │   ├── define-lv.yml
│   │   │   ├── define-vg.yml
│   │   │   ├── docker-set-ip.yml
│   │   │   ├── docker-storage-setup.yml
│   │   │   ├── infra-fs.yml
│   │   │   ├── initial-packages.yml
│   │   │   ├── main.yml
│   │   │   ├── master-fs.yml
│   │   │   ├── remove-vg.yml
│   │   │   ├── ssh-keyscan.yml
│   │   │   └── subscribe-hosts.yml
│   │   └── vars
│   └── openshift-provision
│       ├── README.md
│       ├── TODO.md
│       ├── defaults
│       │   └── main.yml
│       ├── examples
│       │   ├── base-provision
│       │   │   ├── README.md
│       │   │   ├── ansible.cfg
│       │   │   ├── hosts
│       │   │   ├── provision
│       │   │   │   └── base.yml
│       │   │   ├── provision.yml
│       │   │   └── resources
│       │   │       ├── BuildConfig
│       │   │       │   └── rhel7-custom.yml
│       │   │       ├── ClusterRole
│       │   │       │   ├── pod-manager.yml
│       │   │       │   ├── support-l2.yml
│       │   │       │   └── support-l3.yml
│       │   │       └── ImageStream
│       │   │           ├── nodejs.yml
│       │   │           └── rhel7.yml
│       │   ├── cakephp-pipeline
│       │   │   ├── README.md
│       │   │   ├── ansible.cfg
│       │   │   ├── app-build.yml
│       │   │   ├── app-deploy.yml
│       │   │   ├── login-creds.yml.example
│       │   │   ├── mysql.template.yml
│       │   │   ├── pipeline-setup.yml
│       │   │   ├── resources
│       │   │   │   ├── app-buildconfig.yml.j2
│       │   │   │   ├── app-deploymentconfig.yml.j2
│       │   │   │   ├── app-limitrange-large.yml
│       │   │   │   ├── app-limitrange-medium.yml
│       │   │   │   ├── app-limitrange-small.yml
│       │   │   │   ├── app-limitrange-xlarge.yml
│       │   │   │   ├── app-quota.yml.j2
│       │   │   │   ├── app-route.yml.j2
│       │   │   │   └── app-service.yml.j2
│       │   │   └── vars
│       │   │       ├── app-build.yml
│       │   │       ├── app-deploy.yml
│       │   │       └── pipeline-setup.yml
│       │   └── cakephp-template
│       │       ├── README.md
│       │       ├── ansible.cfg
│       │       ├── cakephp-mysql-example.template.yml
│       │       ├── playbook.yml
│       │       └── vars.yml
│       ├── filter_plugins
│       │   ├── change_record.py
│       │   ├── change_record.pyc
│       │   ├── is_array.py
│       │   ├── is_array.pyc
│       │   ├── yaml_to_resource_list.py
│       │   └── yaml_to_resource_list.pyc
│       ├── handlers
│       │   └── main.yml
│       ├── library
│       │   ├── openshift_login.py
│       │   └── openshift_provision.py
│       ├── meta
│       │   └── main.yml
│       ├── tasks
│       │   ├── cluster-resources.yml
│       │   ├── cluster-role-binding.yml
│       │   ├── cluster-role-bindings.yml
│       │   ├── group.yml
│       │   ├── helm-chart.yml
│       │   ├── main.yml
│       │   ├── openshift-3-version-facts.yml
│       │   ├── openshift-4-version-facts.yml
│       │   ├── openshift-cluster-provision.yml
│       │   ├── openshift-cluster.yml
│       │   ├── process-template.yml
│       │   ├── project-imagestreams.yml
│       │   ├── project-multicast.yml
│       │   ├── project-pod-network.yml
│       │   ├── project-resources.yml
│       │   ├── project-role-binding.yml
│       │   ├── project-role-bindings.yml
│       │   ├── project.yml
│       │   └── service-accounts.yml
│       ├── tests
│       │   ├── README.md
│       │   ├── ansible.cfg
│       │   ├── charts
│       │   │   ├── test-chart
│       │   │   │   ├── Chart.yaml
│       │   │   │   ├── templates
│       │   │   │   │   ├── configmap-list.yml
│       │   │   │   │   └── configmap.yml
│       │   │   │   └── values.yaml
│       │   │   └── test-cluster-chart
│       │   │       ├── Chart.yaml
│       │   │       ├── templates
│       │   │       │   ├── storageclass-list.yml
│       │   │       │   └── storageclass.yml
│       │   │       └── values.yaml
│       │   ├── inventory
│       │   ├── login-creds.yml.example
│       │   ├── manual-test-projects-helm-charts.yml
│       │   ├── resources
│       │   │   ├── test-buildconfig.yml.j2
│       │   │   ├── test-clusterresourcequota.yml.j2
│       │   │   ├── test-clusterrole.yml.j2
│       │   │   ├── test-clusterrolebinding.yml.j2
│       │   │   ├── test-configmap.yml.j2
│       │   │   ├── test-daemonset.yml.j2
│       │   │   ├── test-deployment.yml.j2
│       │   │   ├── test-deploymentconfig.yml.j2
│       │   │   ├── test-horizontalpodautoscaler.yml.j2
│       │   │   ├── test-limitrange.yml.j2
│       │   │   ├── test-list-statefulset-service.yml.j2
│       │   │   ├── test-networkpolicy.yml.j2
│       │   │   ├── test-persistentvolume-minimal.yml.j2
│       │   │   ├── test-persistentvolume.yml.j2
│       │   │   ├── test-persistentvolumeclaim-minimal.yml.j2
│       │   │   ├── test-persistentvolumeclaim.yml.j2
│       │   │   ├── test-replicaset.yml.j2
│       │   │   ├── test-replicationcontroller.yml.j2
│       │   │   ├── test-resourcequota.yml.j2
│       │   │   ├── test-role.yml.j2
│       │   │   ├── test-rolebinding.yml.j2
│       │   │   ├── test-route.yml.j2
│       │   │   ├── test-secret.yml.j2
│       │   │   ├── test-securitycontextconstraints.yml.j2
│       │   │   ├── test-service.yml.j2
│       │   │   ├── test-serviceaccount.yml.j2
│       │   │   ├── test-statefulset.yml.j2
│       │   │   ├── test-storageclass-list.yml
│       │   │   ├── test-storageclass-multidoc-list.yml
│       │   │   ├── test-storageclass.yml.j2
│       │   │   └── test-template.yml.j2
│       │   ├── setup-test.yml
│       │   ├── tasks
│       │   │   ├── test-projects-join_pod_networks.yml
│       │   │   └── test-projects-multicast_enabled.yml
│       │   ├── test-cluster-helm_charts.yml
│       │   ├── test-cluster_resources-ClusterResourceQuota.yml
│       │   ├── test-cluster_resources-ClusterRole.yml
│       │   ├── test-cluster_resources-List.yml
│       │   ├── test-cluster_resources-PersistentVolume.yml
│       │   ├── test-cluster_resources-multidoc.yml
│       │   ├── test-cluster_role_bindings.yml
│       │   ├── test-groups.yml
│       │   ├── test-login.yml
│       │   ├── test-openshift_login.yml
│       │   ├── test-openshift_provision-BuildConfig.yml
│       │   ├── test-openshift_provision-ClusterResourceQuota.yml
│       │   ├── test-openshift_provision-ClusterRole.yml
│       │   ├── test-openshift_provision-ClusterRoleBinding.yml
│       │   ├── test-openshift_provision-ConfigMap.yml
│       │   ├── test-openshift_provision-DaemonSet.yml
│       │   ├── test-openshift_provision-Deployment.yml
│       │   ├── test-openshift_provision-DeploymentConfig.yml
│       │   ├── test-openshift_provision-HorizontalPodAutoscaler.yml
│       │   ├── test-openshift_provision-ImageStream.yml
│       │   ├── test-openshift_provision-LimitRange.yml
│       │   ├── test-openshift_provision-NetworkPolicy.yml
│       │   ├── test-openshift_provision-PersistentVolume.yml
│       │   ├── test-openshift_provision-PersistentVolumeClaim.yml
│       │   ├── test-openshift_provision-ReplicaSet.yml
│       │   ├── test-openshift_provision-ReplicationController.yml
│       │   ├── test-openshift_provision-ResourceQuota.yml
│       │   ├── test-openshift_provision-Role.yml
│       │   ├── test-openshift_provision-RoleBinding.yml
│       │   ├── test-openshift_provision-Route.yml
│       │   ├── test-openshift_provision-Secret.yml
│       │   ├── test-openshift_provision-SecurityContextConstraints.yml
│       │   ├── test-openshift_provision-Service.yml
│       │   ├── test-openshift_provision-ServiceAccount.yml
│       │   ├── test-openshift_provision-StatefulSet.yml
│       │   ├── test-openshift_provision-StorageClass.yml
│       │   ├── test-openshift_provision-Template.yml
│       │   ├── test-openshift_provision-patch.yml
│       │   ├── test-projects-basics.yml
│       │   ├── test-projects-helm_charts.yml
│       │   ├── test-projects-imagestreams.yml
│       │   ├── test-projects-join_pod_networks.yml
│       │   ├── test-projects-multicast_enabled.yml
│       │   ├── test-projects-process_templates.yml
│       │   ├── test-projects-resources-BuildConfig.yml
│       │   ├── test-projects-resources-ConfigMap.yml
│       │   ├── test-projects-resources-DaemonSet.yml
│       │   ├── test-projects-resources-Deployment.yml
│       │   ├── test-projects-resources-DeploymentConfig.yml
│       │   ├── test-projects-resources-HorizontalPodAutoscaler.yml
│       │   ├── test-projects-resources-ImageStream.yml
│       │   ├── test-projects-resources-LimitRange.yml
│       │   ├── test-projects-resources-List.yml
│       │   ├── test-projects-resources-PersistentVolumeClaim.yml
│       │   ├── test-projects-resources-ReplicaSet.yml
│       │   ├── test-projects-resources-ReplicationController.yml
│       │   ├── test-projects-resources-ResourceQuota.yml
│       │   ├── test-projects-resources-Route.yml
│       │   ├── test-projects-resources-Secret.yml
│       │   ├── test-projects-resources-Service.yml
│       │   ├── test-projects-resources-ServiceAccount.yml
│       │   ├── test-projects-resources-StatefulSet.yml
│       │   ├── test-projects-resources-Template.yml
│       │   ├── test-projects-role_bindings.yml
│       │   ├── test-projects-service_accounts.yml
│       │   └── test.sh
│       └── vars
│           └── main.yml
├── setup_haproxy_for_apps.yml
├── setup_haproxy_for_master-app.yml
├── setup_haproxy_for_masters.yml
├── setup_keepalived_vip_for_apps.yml
├── setup_keepalived_vip_for_master-app.yml
├── setup_keepalived_vip_for_masters.yml
├── tasks
│   ├── apply-user-quota.yml
│   ├── check-certificates-prereqs.yml
│   ├── get-all-users.yml
│   ├── get-users-by-group.yml
│   ├── label-user-primary-group.yml
│   ├── label-users-primary-group.yml
│   ├── set-cluster-quota-by-primary-group.yml
│   ├── set-sandbox-limitrange.yml
│   ├── set-sandbox-limitranges.yml
│   └── users-group.yml
├── users-groups-management.sh
└── users-groups-management.yml
```
