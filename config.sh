#!/bin/bash
# Abort on error
set -e

CLUSTER=$1
ENVIRON=$(grep ^environ= hosts.$CLUSTER | cut -d= -f2)

# Do a Full OCP Cluster Validation (end-to-end)
ansible-playbook -i hosts.$CLUSTER config.yml
